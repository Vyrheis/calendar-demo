import axios from 'axios'

const basePath = 'https://nexhealth.info/'

export default {
  getAvailability
}

async function getAvailability (date = '2019-11-26T00:00:00-05:00', days = 5) {
  const config = {
    params: {
      // TODO need to prevent axios from encoding +
      // category: '15+Minute+FREE+cosmetic+consult',
      days,
      disp_type: 'location',
      'lids[]': 926,
      'pids[]': 7526475,
      start_date: date,
      subdomain: 'advanced',
      timezone: 'America/Los_Angeles'
    }
  }
  try {
    const response = await axios.get(`${basePath}appointment_slots?category=15+Minute+FREE+cosmetic+consult`, config)
    return response.data
  } catch (e) {
    // TODO
  }
}
